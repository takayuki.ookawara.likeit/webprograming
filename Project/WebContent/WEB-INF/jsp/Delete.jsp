<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<title>ユーザ削除確認</title>
</head>
	<body>
		<form action="" method="post">
			<div class="text-white bg-dark text-right">
				<div class="container">
					<div class="row">
						<div class="col-10">
							${userinfo.name}さん
						</div>
						<div class="col-2">
							<span style="text-decoration: underline;">
								<a href="Logout_servlet">ログアウト</a>
							</span>
						</div>
					</div>
				</div>
			</div>
			<br>
			<h1 class="Center">ユーザ削除確認</h1>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-5">
						ログインID：${userDel.login_id}
					</div>
					<div class="col-4">

					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-5">
						を本当に削除してよろしいでしょうか？
					</div>
					<div class="col-4">

					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-2">
					</div>
					<div class="col-4 text-right">
						<input type="button" onclick="location.href='List_servlet'" value="キャンセル">
					</div>
					<div class="col-4">
						<input type="submit" formaction="Delete_servlet" value="　 ＯＫ 　">
					</div>
					<div class="col-2">
					</div>
				</div>
			</div>
		</form>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<title>ユーザ情報詳細参照</title>
</head>
	<body>
		<form action="" method="post">
			<div class="text-white bg-dark text-right">
				<div class="container">
				<div class="row">
					<div class="col-10">
						${userinfo.name}さん
					</div>
					<div class="col-2">
						<span style="text-decoration: underline;">
							<a href="Logout_servlet">ログアウト</a>
						</span>
					</div>
				</div>
			</div>
			</div>
			<br>
			<h1 class="Center">ユーザ情報詳細参照</h1>
			<br>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						ログインID
					</div>
					<div class="col-6">
						${userFind.login_id}
					</div>
				</div>

			<br>

				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						ユーザ名
					</div>
					<div class="col-6">
						${userFind.name}
					</div>
				</div>

			<br>

				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						生年月日
					</div>
					<div class="col-6">
						${userFind.birth_Date}
					</div>
				</div>

			<br>

				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						登録日時
					</div>
					<div class="col-6">
						${userFind.create_Date}
					</div>
				</div>

			<br>

				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						更新日時
					</div>
					<div class="col-6">
						${userFind.update_Date}
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-2">
					</div>
					<div class="col-10">
						<a href="List_servlet">戻る</a>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>
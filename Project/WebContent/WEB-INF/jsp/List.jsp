<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<title>ユーザ一覧</title>
</head>
	<body>
		<div class="text-white bg-dark text-right">
			<div class="container">
				<div class="row">
					<div class="col-10">
						${userinfo.name}さん
					</div>
					<div class="col-2">
						<span style="text-decoration: underline;">
							<a href="Logout_servlet">ログアウト</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<br>
		<h1 class="Center">ユーザ一覧</h1>
		<div class="container">
			<div class="row">
				<div class="col-10">
				</div>
				<div class="col-2">
					<c:if test="${userinfo.login_id == 'admin'}">
						<a href="Registration_servlet">新規登録</a>
					</c:if>
				</div>
			</div>
		</div>
		<br>
		<form action="List_servlet" method="post">
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						ログインID
					</div>
					<div class="col-6">
						<input type="text" name="login_id" placeholder="未入力です">
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						ユーザ名
					</div>
					<div class="col-6">
						<input type="text" name="name" placeholder="未入力です">
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						生年月日
					</div>
					<div class="col-6">
						<input type="date" name="date" value="年 / 月 / 日"> ～ <input type="date" name="date2" value="年 / 月 / 日">
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-8">
					</div>
					<div class="col-4">
						<input type="submit" value="　　　検索　　　">
					</div>
				</div>
			</div>
		</form>
		<p class="line"><?-- ライン用 --?></p>
		<?-- ここからテーブル --?>
		<div class="container">
			<div class="row">
				<div class="col-2">
				</div>
				<div class="col-8 text-center">
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th>ログインID</th>
								<th>ユーザ名</th>
								<th>生年月日</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${userList}" >
								<tr>
									<th>${user.login_id}</th>
									<th>${user.name}</th>
									<th>${user.birth_Date}</th>
									<th>
										<button onclick="location.href='Detail_servlet?id=${user.id}'" class="btn btn-primary">詳細</button>
										<c:if test="${user.login_id == userinfo.login_id || userinfo.login_id=='admin'}">
											<button onclick="location.href='Update_servlet?id=${user.id}'" class="btn btn-success">更新</button>
										</c:if>
										<c:if test="${userinfo.login_id=='admin'}">
											<button onclick="location.href='Delete_servlet?id=${user.id}'" class="btn btn-danger">削除</button>
										</c:if>
									</th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="col-2">
				</div>
			</div>
		</div>
	</body>
</html>
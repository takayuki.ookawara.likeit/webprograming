<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<title>ログイン画面</title>
</head>
	<body>
		<form class="form-signin" action="Login_servlet" method="post">
			<br>
			<br>
			<h1 class="Center">ログイン画面</h1>
			<br>
			<c:if test="${errMsg !=null}">
				<div class="alert alert-danger" role="alert">
					${errMsg}
				</div>
			</c:if>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-5 text-right">
							ログインID
					</div>
					<div class="col-7">
						<input type="text" name="login_id" placeholder="未入力です" required><br>
					</div>
				</div>
			<br>
				<div class="row">
					<div class="col-5 text-right">
							パスワード
					</div>
					<div class="col-7">
						<input type="password" name="password" placeholder="未入力です" required><br>
					</div>
				</div>
			</div>
			<br>
			<br>
			<div class="center">
				<input type="submit" value="　ログイン　">
			</div>
		</form>
	</body>
</html>
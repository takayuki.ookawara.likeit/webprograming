<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<title>ユーザ情報更新</title>
</head>
	<body>
		<form action="" method="post">
			<div class="text-white bg-dark text-right">
				<div class="container">
					<div class="row">
						<div class="col-10">
							${userinfo.name}さん
						</div>
						<div class="col-2">
							<span style="text-decoration: underline;">
								<a href="Logout_servlet">ログアウト</a>
							</span>
						</div>
					</div>
				</div>
			</div>
			<br>
			<h1 class="Center">ユーザ情報更新</h1>
			<br>
			<br>
			<c:if test="${errMsg !=null}">
				<div class="alert alert-danger" role="alert">
					${errMsg}
				</div>
			</c:if>
			<div class="container">
				<div class="row">
					<div class="col-3">
					</div>
					<div class="col-3 text-center">
						ログインID
					</div>
					<div class="col-6">
						${userFind.login_id}
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						パスワード
					</div>
					<div class="col-6">
						<input type="password" name="password">
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						パスワード（確認）
					</div>
					<div class="col-6">
						<input type="password" name="password2">
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						ユーザ名
					</div>
					<div class="col-6">
						<input type="text" name="name" value="${userFind.name}" required>
					</div>
				</div>
			</div>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-3">

					</div>
					<div class="col-3 text-center">
						生年月日
					</div>
					<div class="col-6">
						<input type="text" name="birth_date" value="${userFind.birth_Date}" required>
					</div>
				</div>
			</div>
			<br>
			<br>
			<p class="center">
				<input type="submit" value="　　更新　　">
			</p>
			<br>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-2">
					</div>
					<div class="col-10">
						<a href="List_servlet">戻る</a>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>
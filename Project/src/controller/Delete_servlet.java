package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Delete_servlet
 */
@WebServlet("/Delete_servlet")
public class Delete_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//ログイン情報取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userinfo");

		/** ログイン情報が取得できない場合 **/
		if(user == null) {
			//強制送還
			response.sendRedirect("Login_servlet");
			return;
		}

		//パラメータ取得
		String id = request.getParameter("id");

		//さがします
		UserDao userDao = new UserDao();
		User userDel = userDao.findUserDel(id);

		//値をセット
		HttpSession atai = request.getSession();
		atai.setAttribute("userDel", userDel);

		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//パラメータ取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userDel");

		//Login_idのみ取得
		String logid = user.getLogin_id();

		//さがして消します
		UserDao userDao = new UserDao();
		userDao.deleteUser(logid);

		//リダイレクト
		response.sendRedirect("List_servlet");
	}

}

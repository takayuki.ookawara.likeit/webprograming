package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Detail_servlet
 */
@WebServlet("/Detail_servlet")
public class Detail_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Detail_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//ログイン情報取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userinfo");

		/** ログイン情報が取得できない場合 **/
		if(user == null) {
			//強制送還
			response.sendRedirect("Login_servlet");
			return;
		}
		//パラメータ取得
		String id = request.getParameter("id");

		//さがします
		UserDao userDao = new UserDao();
		User userFind = userDao.findUserDetails(id);

		//値をセット
		HttpSession atai = request.getSession();
		atai.setAttribute("userFind", userFind);

		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Details.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リダイレクト
		response.sendRedirect("List_servlet");
	}

}

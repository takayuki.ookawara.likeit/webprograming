package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class List_servlet
 */
@WebServlet("/List_servlet")
public class List_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public List_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログイン情報取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userinfo");

		/** ログイン情報が取得できない場合 **/
		if(user == null) {
			//強制送還
			response.sendRedirect("Login_servlet");
			return;
		}
		//取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();
		//set
		request.setAttribute("userList", userList);
		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/List.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//値の取得
		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String date = request.getParameter("date");
		String date2 = request.getParameter("date2");

		//検索
		UserDao userDao = new UserDao();
		List<User> userList = userDao.ListSearchUser(login_id,name,date,date2);
		//セット
		request.setAttribute("userList", userList);

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/List.jsp");
		dispatcher.forward(request, response);
	}

}

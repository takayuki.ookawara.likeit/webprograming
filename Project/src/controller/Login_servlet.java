package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Login_servlet
 */
@WebServlet("/Login_servlet")
public class Login_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログイン確認
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userinfo");

		//ログイン情報が取得できない場合
		if(user == null) {
			//ふぉわーど
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
		}
		//ログインしていた場合
		response.sendRedirect("List_servlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		//入力値の取得
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");

		//さがします
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(login_id,password);

		/** ログイン失敗時 **/
		if(user == null) {
			//エラーメッセージ
			request.setAttribute("errMsg", "ログインID、またはパスワードが異なります");
			//Loginで拘留
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** ログイン成功時 **/
		//値をセット
		HttpSession session = request.getSession();
		session.setAttribute("userinfo", user);
		//リダイレクト
		response.sendRedirect("List_servlet");

	}

}

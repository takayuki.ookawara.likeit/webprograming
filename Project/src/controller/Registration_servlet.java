package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Registration_servlet
 */
@WebServlet("/Registration_servlet")
public class Registration_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//ログイン情報取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userinfo");

		/** 管理者 **/
		String loginid = user.getLogin_id();
		if(loginid.equals("admin")) {
			//遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Registration.jsp");
			dispatcher.forward(request, response);
			return;
		}
		/** その他 **/
		//強制送還
		response.sendRedirect("List_servlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//値の取得とか
		UserDao userDao = new UserDao();
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");


		/** パスワード確認 **/
		//パスワードが異なる場合
		if(!(password.equals(password2))){
			request.setAttribute("errMsg", "入力されたパスワードが異なります");
			//拘留
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 重複確認 **/
		//呼び出して重複確認
		User otazune = userDao.chohukuUser(login_id);
		//重複していた場合
		if(!(otazune == null)){
			request.setAttribute("errMsg", "既に登録されています。");
			//拘留
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Registration.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 登録 **/
		//登録
		userDao.createUser(login_id,name,birth_date,password);
		//リダイレクト
		response.sendRedirect("List_servlet");
	}
}

package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Update_servlet
 */
@WebServlet("/Update_servlet")
public class Update_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Update_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//ログイン情報取得
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userinfo");

		/** ログイン情報が取得できない場合 **/
		if(user == null) {
			//強制送還
			response.sendRedirect("Login_servlet");
			return;
		}
		//パラメータ取得
		String id = request.getParameter("id");

		//さがします
		UserDao userDao = new UserDao();
		User userFind = userDao.findUserDetails(id);

		//値をセット
		HttpSession atai = request.getSession();
		atai.setAttribute("userFind", userFind);

		//遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		//値の取得
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		//ユーザ情報
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userFind");
		String login_id = user.getLogin_id();

		//パスワードが入力されていない場合
		if(password == "" && password2 == "") {
			//パス抜き更新
			UserDao userDao = new UserDao();
			userDao.updateUser(login_id,name,birth_date);
			//リダイレクト
			response.sendRedirect("List_servlet");
			return;

		//パスワードが入力されており、正しい場合
		}else if(password.equals(password2)){
			//更新
			UserDao userDao = new UserDao();
			userDao.updateUserP(login_id,name,birth_date,password);
			//リダイレクト
			response.sendRedirect("List_servlet");
			return;

		//パスワードが異なる場合、もしくはよく分からないやつ全部ここ
		}else {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			//拘留
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

}

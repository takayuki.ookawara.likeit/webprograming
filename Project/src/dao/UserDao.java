package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//ログイン情報 返しはloginIDとユーザ名 もしくはnull
	public User findByLoginInfo(String login_id,String password) {
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "select * from user where login_id=? and password=?";

			//encode
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			//生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, login_id);
			stmt.setString(2, result);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			//成功時
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		}catch(SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//デリートする時に探すやつ
	public User findUserDel(String id) {
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "select * from user where id=?";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			//成功時
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//探して単体ユーザの全情報物故抜くやつ
	public User findUserDetails(String id) {
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "select * from user where id=?";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			//成功時
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String birth_Date = rs.getString("birth_date");
			String create_Date = rs.getString("create_date");
			String update_Date = rs.getString("update_date");
			return new User(loginIdData, nameData,birth_Date,create_Date,update_Date);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全探し あれーりすと
	public List<User> findAll() {
		Connection con = null;
        List<User> userList = new ArrayList<User>();
        try {
	        con=DBmanager.getConnection();
	        String sql = "select * from user where login_id != 'admin'";

	        Statement stmt = con.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

	        while (rs.next()) {
	        	int id = rs.getInt("id");
	        	String loginId = rs.getString("login_id");
	        	String name = rs.getString("name");
	        	String birthDate = rs.getString("birth_date");
	        	String password = rs.getString("password");
	        	String createDate = rs.getString("create_date");
	        	String updateDate = rs.getString("update_date");
	        	User user = new User(id, loginId,name,birthDate,password,createDate,updateDate);

                userList.add(user);
	        }
        }catch(SQLException e){
        	e.printStackTrace();
        }finally{
        	if(con != null){
        		 try {
                     con.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                     return null;
                 }
        	}
        }
        return userList;
	}

	//任意のユーザを探すシリーズ

	//リスト
	public List<User> ListSearchUser(String login_id,String name,String date,String date2) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();
		try {
			//接続など
			con = DBmanager.getConnection();
			Statement stmt = con.createStatement();


			//SQL ベース
			String sql = "select * from user where login_id != 'admin'";

			//loginID
			if(!(login_id.isEmpty())) {
				sql = sql + "and login_id = '"+login_id+"'";
			}
			//name
			if(!(name.isEmpty())) {
				sql = sql + "and name like '%"+name+"%'";
			}
			//date
			if(!(date.isEmpty())) {
				sql = sql + "and birth_date >= '"+date+"'";
			}
			//date2
			if(!(date2.isEmpty())) {
				sql = sql + "and birth_date <= '"+date2+"'";
			}

			ResultSet rs = stmt.executeQuery(sql);

	        while (rs.next()) {
	        	int id = rs.getInt("id");
	        	String loginId = rs.getString("login_id");
	        	String nameD = rs.getString("name");
	        	String birthDate = rs.getString("birth_date");
	        	String password = rs.getString("password");
	        	String createDate = rs.getString("create_date");
	        	String updateDate = rs.getString("update_date");
	        	User user = new User(id, loginId,nameD,birthDate,password,createDate,updateDate);

                userList.add(user);
	        }
        }catch(SQLException e){
        	e.printStackTrace();
        }finally{
        	if(con != null){
        		 try {
                     con.close();
                 } catch (SQLException e) {
                     e.printStackTrace();
                     return null;
                 }
        	}
        }
        return userList;
	}
	//ユーザ削除
	public void deleteUser(String login_id){
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "delete from user where login_id =?";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, login_id);
			stmt.executeUpdate();

			con.commit();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザ作成
	public void createUser(String login_id,String name,String birth_date,String password){
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "insert into user (login_id,name,birth_date,password) values (?,?,?,?)";

			//encode
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			//生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, login_id);
			stmt.setString(2, name);
			stmt.setString(3, birth_date);
			stmt.setString(4, result);
			stmt.executeUpdate();

			con.commit();

		}catch(SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	//ユーザ更新
	public void updateUser(String login_id,String name,String birth_date){
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "update user set name = ? ,birth_date = ? where login_id = ?;";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, birth_date);
			stmt.setString(3, login_id);
			stmt.executeUpdate();

			con.commit();

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザ更新
	public void updateUserP(String login_id,String name,String birth_date,String password){
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "update user set password = ? ,name = ? ,birth_date = ? where login_id = ?;";

			//encode
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			//生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, result);
			stmt.setString(2, name);
			stmt.setString(3, birth_date);
			stmt.setString(4, login_id);
			stmt.executeUpdate();

			con.commit();

		}catch(SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ユーザ重複確認
	public User chohukuUser(String login_id){
		Connection con = null;
		try {
			//接続
			con = DBmanager.getConnection();
			//SQL
			String sql = "select * from user where login_id = ?";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, login_id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			//成功時
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}

/* databese 作成 */
create database webpro_kadai DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

/* 鯖使用宣言 */
use webpro_kadai;

/* テーブル作成 - user */
create table user (
id int primary key auto_increment unique not null,
login_id varchar(255) unique not null,
name varchar(255) not null,
birth_date date not null,
password varchar(255) not null,
create_date datetime DEFAULT CURRENT_TIMESTAMP,
update_date datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

/*
idは自動採番
date系列は勝手に追加される
これら以外はデータ入力すべし
*/

/* 新規登録（普段用） */
insert into user (login_id,name,birth_date,password) values (?,?,?,?);

/* 新規登録（IDも指定する場合） */
insert into user (id,login_id,name,birth_date,password) values (?,?,?,?,?);

/* 更新時 */
update user set /* 変えたい項目 */ = /* 変更内容 */ where id = /*変えたい奴*/;

/* 削除 */
delete from user where login_id =?;

/* 名称変更呼び出し */
user.id as ID
user.login_id  as ログインID
user.name as 名前
user.birth_date as 生年月日
user.password as パスワード
user.create_date as 作成日時
user.update_date as 更新日時

